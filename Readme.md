﻿C# application that measures weight using the WiiFit board and posts the value to a Google spreadsheet for recording.

```
#!xml
<?xml version="1.0" encoding="utf-8" ?>
<configuration>
  <appSettings>
    <add key="GoogleDocUrl" value=""/>
    <add key="EntityID" value=""/>
  </appSettings>
</configuration>
```