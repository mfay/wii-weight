﻿namespace WiiWeight
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.btnSubmitWeight = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.calibrate = new System.Windows.Forms.Timer(this.components);
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(13, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(94, 31);
            this.label1.TabIndex = 0;
            this.label1.Text = "0.0 lbs";
            // 
            // btnSubmitWeight
            // 
            this.btnSubmitWeight.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSubmitWeight.Location = new System.Drawing.Point(19, 72);
            this.btnSubmitWeight.Name = "btnSubmitWeight";
            this.btnSubmitWeight.Size = new System.Drawing.Size(139, 36);
            this.btnSubmitWeight.TabIndex = 1;
            this.btnSubmitWeight.Text = "Submit Weight";
            this.btnSubmitWeight.UseVisualStyleBackColor = true;
            this.btnSubmitWeight.Click += new System.EventHandler(this.btnSubmitWeight_Click);
            // 
            // timer1
            // 
            this.timer1.Interval = 3000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // calibrate
            // 
            this.calibrate.Tick += new System.EventHandler(this.calibrate_Tick);
            // 
            // Main
            // 
            this.AcceptButton = this.btnSubmitWeight;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(213, 129);
            this.Controls.Add(this.btnSubmitWeight);
            this.Controls.Add(this.label1);
            this.Name = "Main";
            this.Text = "Wii Weight";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnSubmitWeight;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Timer calibrate;
    }
}

