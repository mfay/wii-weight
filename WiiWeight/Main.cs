﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using WiimoteLib;
using System.Net;
using System.IO;
using System.Configuration;

namespace WiiWeight
{
    public partial class Main : Form
    {
        Wiimote mote = new Wiimote();
        private delegate void WiiChangedDelegate(WiimoteChangedEventArgs args);
        float lastWeight;
        float tare = 0f;
        int calibrateCounter = 0;
        int count = 25;
        bool calibrating = true;

        public Main()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            btnSubmitWeight.Enabled = false;
            mote.WiimoteChanged += OnChange;
            mote.Connect();
            calibrate.Start();
        }

        private void OnChange(object sender, WiimoteChangedEventArgs args)
        {
            if (args.WiimoteState.ExtensionType != ExtensionType.BalanceBoard) return;

            try
            {
                BeginInvoke(new WiiChangedDelegate(UpdateUI), args);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        private void UpdateUI(WiimoteChangedEventArgs args)
        {
            lastWeight = args.WiimoteState.BalanceBoardState.WeightLb;
            if (!calibrating)
            {
                lastWeight = (float) Math.Round(lastWeight - tare, 1);
                if (lastWeight < 0f) lastWeight = 0f;
            }
            label1.Text = string.Format("{0:0.0} lbs", lastWeight);
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            // post the current weight
            timer1.Stop();
            PostWeight(lastWeight);
        }

        private void PostWeight(float weight)
        {
            var url = ConfigurationManager.AppSettings["GoogleDocUrl"];
            var entityID = ConfigurationManager.AppSettings["EntityID"];
            var data = string.Format("{0}.1710168094={1}", entityID, weight);
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "POST";

            byte[] binaryData = ASCIIEncoding.ASCII.GetBytes(data);
            request.ContentType = "application/x-www-form-urlencoded";
            request.ContentLength = binaryData.Length;
            var stream = request.GetRequestStream();

            stream.Write(binaryData, 0, binaryData.Length);
            stream.Close();

            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            Console.WriteLine(new StreamReader(response.GetResponseStream()).ReadToEnd());
            Console.WriteLine(response.StatusCode);
        }

        private void btnSubmitWeight_Click(object sender, EventArgs e)
        {
            timer1.Start();
        }

        private void calibrate_Tick(object sender, EventArgs e)
        {
            if (lastWeight == 0F) return;

            tare += lastWeight;
            Console.WriteLine("tare: {0:0.000}", tare);
            if (++calibrateCounter > count)
            {
                calibrate.Stop();
                btnSubmitWeight.Enabled = true;
                Console.WriteLine("TOTAL TARE: {0:0.000}", tare);
                tare /= count;
                Console.WriteLine("FINAL TARE: {0:0.000}", tare);
                calibrating = false;
            }
        }
    }
}
